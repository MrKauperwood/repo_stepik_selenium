from selenium import webdriver
import time
import math

link = "http://suninjuly.github.io/alert_accept.html"

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

try:
    browser = webdriver.Chrome()
    browser.get(link)

    button = browser.find_element_by_class_name("btn")
    button.click()

    alert = browser.switch_to.alert
    alert.accept()

    x = browser.find_element_by_id("input_value")
    x = int(x.text)
    res = calc(x)

    input_string = browser.find_element_by_id("answer")
    input_string.send_keys(res)

    button = browser.find_element_by_tag_name("button")
    button.click()

finally:
    time.sleep(10)
    browser.quit()