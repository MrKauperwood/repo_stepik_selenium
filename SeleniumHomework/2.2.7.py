from selenium import webdriver
import time
import os

link = "http://suninjuly.github.io/file_input.html"

try:
    browser = webdriver.Chrome()
    browser.get(link)

    name = browser.find_element_by_css_selector('[name="firstname"]')
    name.send_keys("Алексей")

    last_name = browser.find_element_by_css_selector('[name="lastname"]')
    last_name.send_keys("Бондаренко")

    email = browser.find_element_by_css_selector('[name="email"]')
    email.send_keys("lbrec@gmail.com")

    current_dir = os.path.abspath(os.path.dirname(__file__))
    file_path = os.path.join(current_dir, 'test.txt')

    print(file_path)

    import_file = browser.find_element_by_id("file")
    import_file.send_keys(file_path)

    button = browser.find_element_by_class_name("btn")
    button.click()

finally:
    time.sleep(10)
    browser.quit()