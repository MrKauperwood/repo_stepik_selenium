from selenium import webdriver
import time
import math

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

link = r"http://suninjuly.github.io/math.html"

try:
    browser = webdriver.Chrome()
    browser.get(link)

    get_value = browser.find_element_by_css_selector("#input_value")
    print(get_value.text)
    ans = calc(int(get_value.text))

    get_input = browser.find_element_by_css_selector("#answer")
    get_input.send_keys(ans)

    check_one = browser.find_element_by_css_selector("#robotCheckbox")
    check_one.click()

    radio_one = browser.find_element_by_css_selector("#robotsRule")
    radio_one.click()

    button = browser.find_element_by_class_name("btn-default")
    button.click()



finally:
    time.sleep(20)
    browser.quit()