from selenium import webdriver
import time

link = "http://suninjuly.github.io/selects1.html"

try:
    browser = webdriver.Chrome()
    browser.get(link)

    num1 = browser.find_element_by_id("num1").text
    num2 = browser.find_element_by_id("num2").text
    sum = int(num1) + int(num2)

    browser.find_element_by_tag_name("select").click()
    browser.find_element_by_css_selector("[value=\"{}\"]".format(sum)).click()

    browser.find_element_by_class_name("btn").click()

finally:
    time.sleep(10)
    browser.quit()