from selenium import webdriver
import time
import math

link = "http://suninjuly.github.io/redirect_accept.html"

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

try:
    browser = webdriver.Chrome()
    browser.get(link)

    button = browser.find_element_by_class_name("btn")
    button.click()

    new_win = browser.window_handles[1]
    browser.switch_to_window(new_win)

    num = browser.find_element_by_id("input_value")
    num_int = int(num.text)
    ans = calc(num_int)

    answer = browser.find_element_by_id("answer")
    answer.send_keys(ans)

    button2 = browser.find_element_by_class_name("btn")
    button2.click()


finally:
    time.sleep(10)
    browser.quit()