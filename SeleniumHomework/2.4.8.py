from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import math

link = "http://suninjuly.github.io/explicit_wait2.html"

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

try:
    browser = webdriver.Chrome()
    browser.get(link)

    # говорим Selenium проверять цену до тех пор, пока она не станет равно 100
    price = WebDriverWait(browser, 12).until(EC.text_to_be_present_in_element((By.ID, "price"), "$100"))

    button = browser.find_element(By.ID, "book")
    button.click()

    x = browser.find_element_by_id("input_value")
    x = int(x.text)
    res = calc(x)

    input_string = browser.find_element_by_id("answer")
    browser.execute_script("return arguments[0].scrollIntoView(true);", input_string)
    input_string.send_keys(res)

    button2 = browser.find_element(By.ID, "solve")
    button2.click()

finally:
    time.sleep(50)
    browser.quit()