from selenium import webdriver
import time
import math

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))

link = r"http://suninjuly.github.io/get_attribute.html"

try:
    browser = webdriver.Chrome()
    browser.get(link)

    get_value = browser.find_element_by_css_selector("#treasure")
    value = get_value.get_attribute("valuex")
    print(value)
    ans = calc(int(value))

    get_input = browser.find_element_by_css_selector("#answer")
    get_input.send_keys(ans)

    check_one = browser.find_element_by_css_selector("#robotCheckbox")
    check_one.click()

    radio_one = browser.find_element_by_css_selector("#robotsRule")
    radio_one.click()

    button = browser.find_element_by_class_name("btn-default")
    button.click()



finally:
    time.sleep(20)
    browser.quit()